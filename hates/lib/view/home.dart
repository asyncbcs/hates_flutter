import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Location location = new Location();
  GoogleMapController mapController;
  Marker marker;
  Position userLocation;
  double latitud = 0;
  double longitud = 0;
  var buscarDir;
  bool mostrarCard = false;
  var height = 0.0;
  var heightmin = 0.0;
  var heightmax = 120.0;

  CameraPosition _initialPosition = CameraPosition(
    target: LatLng(24.153512, -110.309349),
    zoom: 15.0,
  );

  List<Marker> marcadores = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getLocation();
    marcadores.add(
      Marker(
        markerId: MarkerId('MiMarcador'),
        draggable: false,
        consumeTapEvents: true,
        icon: BitmapDescriptor.fromAsset("assets/hate.png"),
        position: LatLng(24.153512, -110.309349),
        onTap: () {
          setState(() {
            if (height == heightmax) {
              height = heightmin;
              mostrarCard = false;
            } else {
              height = heightmax;
              Timer(Duration(milliseconds: 120), () {
                setState(() {
                  mostrarCard = true;
                });
              });
            }
          });
          moverCamara(CameraPosition(
            target: LatLng(24.153512, -110.309349),
            zoom: 16.0,
          ));
        },
        infoWindow: InfoWindow(title: "Hola", snippet: "como estás"),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          GoogleMap(
            onMapCreated: onMapCreated,
            initialCameraPosition: _initialPosition,
            myLocationEnabled: true,
            mapType: MapType.normal,
            markers: Set.from(marcadores),
            compassEnabled: false,
          ),
          Positioned(
            top: 46.0,
            right: 16.0,
            left: 16.0,
            child: Container(
              height: 50.0,
              width: double.infinity,
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      blurRadius: 20.0,
                      offset: Offset(
                        0.0,
                        10.0,
                      ),
                    )
                  ],
                  borderRadius: BorderRadius.circular(25.0),
                  color: Colors.white),
              child: TextField(
                decoration: InputDecoration(
                  hintText: 'Ingrese direccion a buscar',
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(left: 24.0, top: 15.0),
                  suffixIcon: IconButton(
                    icon: Icon(Icons.search),
                    onPressed: barraBusqueda,
                    iconSize: 24.0,
                  ),
                ),
                onChanged: (val) {
                  setState(() {
                    buscarDir = val;
                  });
                },
              ),
            ),
          ),
          mostrarInfo(),
        ],
      ),
    );
  }

  Widget mostrarInfo() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          AnimatedContainer(
            duration: Duration(milliseconds: 120),
            height: height,
            width: double.infinity,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.black45,
                  blurRadius: 20.0,
                  offset: Offset(
                    0.0,
                    10.0,
                  ),
                ),
              ],
              borderRadius: BorderRadius.only(
                topLeft: const Radius.circular(36.0),
                topRight: const Radius.circular(36.0),
              ),
              color: Colors.white,
            ),
            child: Visibility(
              visible: mostrarCard,
              child: Container(
                child: Padding(
                  padding:
                      const EdgeInsets.only(left: 24.0, right: 24.0, top: 16.0),
                  child: Column(
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          setState(() {
                            if (height == heightmax) {
                              height = heightmin;
                              mostrarCard = false;
                            }
                          });
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 4.0, right: 4.0, top: 2.0, bottom: 2.0),
                          child: Container(
                            height: 6,
                            width: 30,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(36.0)),
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            height: 70,
                            width: 70,
                            decoration: new BoxDecoration(
                              color: Colors.orange,
                              shape: BoxShape.circle,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _getLocation() async {
    LocationData currentLocation;
    currentLocation = await location.getLocation();
    latitud = currentLocation.latitude;
    longitud = currentLocation.longitude;
    posicionActual();
  }

  posicionActual() {
    _initialPosition =
        CameraPosition(target: LatLng(latitud, longitud), zoom: 15.0);
    moverCamara(_initialPosition);
  }

  barraBusqueda() {
    Geolocator().placemarkFromAddress(buscarDir).then((result) {
      mapController.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: LatLng(
              result[0].position.latitude,
              result[0].position.longitude,
            ),
            zoom: 10.0,
          ),
        ),
      );
    });
  }

  void onMapCreated(GoogleMapController controller) {
    _getLocation();
    setState(() {
      mapController = controller;
    });
  }

  void moverCamara(CameraPosition cameraPosition) {
    if (mapController != null) {
      mapController
          .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
    }
  }
}
